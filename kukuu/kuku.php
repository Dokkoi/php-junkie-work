<!doctype html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <title>九九</title>
  <style>
    th,td{
      border:solid 1px #aaa;
    }
    .gray {
      background-color: #808080;
    }
  </style>
</head>
<body>
  <table>
    <caption>九九やるよー</caption>
    <?php $kuku_list = [1, 2, 3, 4, 5, 6, 7, 8, 9]; ?>
    <tr>
      <th></th>
    <?php foreach($kuku_list as $num): ?>
      <th><?php echo $num; ?></th>
    <?php endforeach; ?>
    </tr>
    <?php foreach($kuku_list as $left_num): ?>
    <tr>
      <td><?php echo $left_num; ?></td>
      <?php foreach($kuku_list as $right_num): ?>
        <?php $result_num = $left_num * $right_num; ?>
        <td <?php echo $result_num % 2 === 0 ? 'class="gray"' : ''; ?>>
          <?php echo $result_num; ?>
        </td>
      <?php endforeach; ?>
    </tr>
    <?php endforeach; ?>
  </table>
</body>
</html>