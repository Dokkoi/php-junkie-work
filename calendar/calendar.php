<!doctype html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <title>カレンダー</title>
  <style>
    th,td{
      border:solid 1px #aaa;
    }
    #header {
      display: flex;
    }
    #title {
      margin: 0 10px;
    }
  </style>
</head>
<body>
  <div id="calendar-box">
    <?php
      if(isset($_GET["year"]) && isset($_GET["month"])):
        $year_month = $_GET["year"].'-'.$_GET["month"];
      else:
        $year_month = 'this month';
      endif;

      $this_month = new DateTime("first day of $year_month");

      $last_month = new DateTime("first day of $year_month");
      $last_month->modify("-1 month");

      $next_month = new DateTime("first day of $year_month");
      $next_month->modify("+1 month");
    ?>
    <div id="header">
      <a href="/calendar.php?year=<?php echo $last_month->format('Y'); ?>&month=<?php echo $last_month->format('m'); ?>">前月へ</a>
      <strong id="title"><?php echo $this_month->format('Y年m月d日') ?></strong>
      <a href="/calendar.php?year=<?php echo $next_month->format('Y'); ?>&month=<?php echo $next_month->format('m'); ?>">翌月へ</a>
    </div>

    <div id="calendar">
      <table>
        <?php $day_of_week_list = ['日', '月', '火', '水', '木', '金', '土']; ?>
        <tr>
        <?php foreach($day_of_week_list as $day_of_week): ?>
          <th><?php echo $day_of_week; ?></th>
        <?php endforeach; ?>
        </tr>

        <?php
          // 月初の曜日を確定させるために用意
          $first_date = new DateTime('first day of ' . $year_month);
          // 月末の日付を確定させるために用意
          $last_date = new DateTime('last day of ' . $year_month);

          // 月初・月末の曜日確定 : 0なら日曜、1なら月曜、、6なら土曜
          $first_day_of_week = (int)$first_date->format('w');

          //　2月がうるう年かそうでないかで変わるだけの数値、他の月は全て5週間
          $week_loop_limit = (int)$last_date->format('d') / 7 > 4 ? 5 : 4;
          $day = 1;
          $first_day_flg = false;
          $last_day_flg = false;
        ?>

        <?php for($loop_cnt = 1; $loop_cnt <= $week_loop_limit; $loop_cnt++): ?>
        <tr>
          <?php for($day_loop_cnt = 0; $day_loop_cnt <= 6; $day_loop_cnt++): ?>
            <?php
              if(!$first_day_flg && $first_day_of_week === $day_loop_cnt):
                $first_day_flg = true;
              endif;
            ?>

            <?php if(($first_day_flg && $last_day_flg) || (!$first_day_flg && !$last_day_flg)): ?>
              <td></td>
            <?php else: ?>
              <td><?php echo $day; ?></td>
            <?php endif; ?>

            <?php
              if(!$last_day_flg && (int)$last_date->format('d') === $day):
                $last_day_flg = true;
              endif;

              if($first_day_flg && !$last_day_flg):
                $day++;
              endif;
            ?>
          <?php endfor; ?>
        </tr>
        <?php endfor; ?>
      </table>
    </div>
  </div>
</body>
</html>