# 九九あーんどカレンダー

## URL
https://php-junkie.net/beginner/how_to_study_1/#play

## 九九

### 要件
・for文・while文・foreach文のいずれかを使って、九九表を作る
・偶数にのみ背景色をつける

### 動作確認

```
$ cd kukuu/
$ php -S localhost:8000
$ open http://localhost:8000/kuku.php
```

##　カレンダー

### 要件
・date関数かDatetimeクラスを使って、カレンダーを作る
・初期値は今月のカレンダーを表示する
・「前月へ」「翌月へ」というリンクを用意し、クリックすると表示中の月の前月や翌月を表示する

### 動作確認

```
$ cd calendar/
$ php -S localhost:8000
$ open http://localhost:8000/calendar.php

# 日付指定するなら
$ open http://localhost:8000/calendar.php?year=2016&month=02
```



